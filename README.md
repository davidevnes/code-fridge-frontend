## Code Fridge - Test landing page

The cloned landing page of GitHub Marketplace built with React, Redux, Redux-Saga, Material-UI, and SCSS.

## Project Status

This project has just one page - the landing page.

Also, the UI is cloned from GitHub Marketplace landing page.

For now, it's not responsive to multiple devices and just for desktop-only.

And there is no API endpoint for this project so I couldn't handle APIs using GraphQL.

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `yarn` installed globally on your machine.

Installation:

`yarn`

To Run Test Suite:

`yarn test`

To Start Server:

`yarn start`

To Visit App:

`localhost:3000/`

## Reflection

I started this project by using the `create-react-app --template redux` boilerplate which takes advantage of React Redux's integration with React components, then adding `material-ui` for the UI library and `storybook` to test components interactively.

And I used Component-Centric File Layout to build components.

Thanks to the benefit of modular CSS, we don’t have to worry about duplicate class names between files, and hence have more freedom with our class names.

The technologies implemented in this project are React, React-Router, Redux, Redux-Saga, LoDash, Material-UI, storybook, and a significant amount of SCSS.
