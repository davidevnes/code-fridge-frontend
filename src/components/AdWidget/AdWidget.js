import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/CustomButton';

import styles from './AdWidget.module.scss';

const AdWidget = ({ imagePath, title, description, buttonValue }) => {
  return (
    <div className={styles.adWidget}>
      <img className={styles.image} src={imagePath} alt="ad-widget" />
      <div className={styles.container}>
        <h3 className={styles.title}>{title}</h3>
        <span className={styles.description}>{description}</span>
      </div>
      <Button buttonType="secondary">{buttonValue}</Button>
    </div>
  );
};

AdWidget.propTypes = {
  imagePath: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  buttonValue: PropTypes.string,
};

AdWidget.defaultProps = {
  imagePath: null,
  title: null,
  description: null,
  buttonValue: null,
};

export default AdWidget;
