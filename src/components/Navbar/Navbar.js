import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';

import styles from './Navbar.module.scss';

const Navbar = () => (
  <div className={styles.navbar}>
    <h3 className={styles.title}>Code Fridge - Test landing page</h3>
    <Tooltip title="David Antunes">
      <Avatar alt="Remy Sharp" className={styles.avatar}>
        DA
      </Avatar>
    </Tooltip>
  </div>
);

export default Navbar;
