import React from 'react';
import PropTypes from 'prop-types';
import { v1 as uuid } from 'uuid';
import Grid from '@material-ui/core/Grid';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

import styles from './Panel.module.scss';

const Panel = ({ title, description, columns, spacing, children, link }) => {
  const elements = React.Children.toArray(children);
  return (
    <div className={styles.panel}>
      <h3 className={styles.title}>{title}</h3>
      <div className={styles.description}>{description}</div>
      <Grid container spacing={spacing}>
        {elements.map((element) => (
          <Grid key={uuid()} item xs={12 / columns}>
            {element}
          </Grid>
        ))}
      </Grid>
      {link && (
        <a href={link} className={styles.link}>
          View all
          <KeyboardArrowRightIcon />
        </a>
      )}
    </div>
  );
};

Panel.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  columns: PropTypes.number,
  spacing: PropTypes.number,
  link: PropTypes.string,
  children: PropTypes.node,
};

Panel.defaultProps = {
  title: null,
  description: null,
  spacing: 3,
  columns: 2,
  link: null,
  children: null,
};

export default Panel;
