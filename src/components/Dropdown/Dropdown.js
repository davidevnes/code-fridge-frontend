import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import styles from './Dropdown.module.scss';

const Dropdown = ({ title, items }) => {
  const [opened, setOpened] = useState(false);
  return (
    <div className={styles.wrapper}>
      <h3 className={styles.title} onClick={() => setOpened(!opened)}>
        {title}
        <button
          type="button"
          className={clsx(styles.toggle, opened && styles.open)}
        >
          <KeyboardArrowDownIcon />
        </button>
      </h3>
      {opened && (
        <ul className={styles.dropdown}>
          {items.map((item) => (
            <li key={item}>
              <a href={`/${item}`}>{item}</a>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

Dropdown.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.string),
};

Dropdown.defaultProps = {
  title: '',
  items: [],
};

export default Dropdown;
