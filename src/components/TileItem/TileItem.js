import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

import styles from './TileItem.module.scss';

const TileItem = ({
  badgeImage,
  badgeColor,
  title,
  comment,
  verfied,
  link,
}) => (
  <a href={link} className={styles.wrapper}>
    <div className={styles.tileItem}>
      <div className={styles.tileBadge} style={{ background: badgeColor }}>
        <img src={badgeImage} alt="tile-badge" />
      </div>
      <section className={styles.section}>
        <h3 className={styles.title}>
          {title}
          {verfied && (
            <Tooltip title="Verified by GitHub" placement="top">
              <VerifiedUserIcon className={styles.verifiedIcon} />
            </Tooltip>
          )}
        </h3>
        <p className={styles.comment}>{comment}</p>
      </section>
    </div>
  </a>
);

TileItem.propTypes = {
  badgeImage: PropTypes.string,
  badgeColor: PropTypes.string,
  title: PropTypes.string,
  comment: PropTypes.string,
  verfied: PropTypes.bool,
  link: PropTypes.string,
};

TileItem.defaultProps = {
  badgeImage: null,
  badgeColor: null,
  title: null,
  comment: null,
  verfied: false,
  link: null,
};

export default TileItem;
