import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';

import styles from './CustomButton.module.scss';

const CustomButton = ({ variant, buttonType, children, ...otherProps }) => {
  return (
    <Button
      className={clsx(styles.button, styles[buttonType], variant)}
      {...otherProps}
    >
      {children}
    </Button>
  );
};

CustomButton.propTypes = {
  variant: PropTypes.string,
  buttonType: PropTypes.oneOf(['primary', 'secondary']),
  children: PropTypes.node,
};

CustomButton.defaultProps = {
  variant: '',
  buttonType: 'primary',
  children: null,
};

export default CustomButton;
