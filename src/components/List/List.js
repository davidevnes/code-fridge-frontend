import React from 'react';
import PropTypes from 'prop-types';

import styles from './List.module.scss';

const List = ({ title, items }) => (
  <div className={styles.wrapper}>
    <h3 className={styles.title}>{title}</h3>
    <ul className={styles.list}>
      {items.map((item) => (
        <li key={item}>
          <a href={`/${item}`}>{item}</a>
        </li>
      ))}
    </ul>
  </div>
);

List.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.string),
};

List.defaultProps = {
  title: '',
  items: [],
};

export default List;
