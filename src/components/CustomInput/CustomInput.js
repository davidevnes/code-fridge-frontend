import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import styles from './CustomInput.module.scss';

const CustomInput = ({ variant, disableSearch, ...otherProps }) => {
  return (
    <Paper className={clsx([styles.wrapper, variant])}>
      {!disableSearch && (
        <IconButton type="submit" className={styles.button} aria-label="search">
          <SearchIcon />
        </IconButton>
      )}
      <InputBase className={styles.input} {...otherProps} />
    </Paper>
  );
};

CustomInput.propTypes = {
  variant: PropTypes.string,
  disableSearch: PropTypes.bool,
};

CustomInput.defaultProps = {
  variant: '',
  disableSearch: false,
};

export default CustomInput;
