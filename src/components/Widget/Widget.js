import React from 'react';
import PropTypes from 'prop-types';

import styles from './Widget.module.scss';

const Widget = ({ background, badgeImage, badgeColor, title, link }) => {
  return (
    <a href={link} className={styles.wrapper}>
      <div className={styles.widget} style={{ background }}>
        <div className={styles.badge} style={{ backgroundColor: badgeColor }}>
          <img className={styles.badgeImage} src={badgeImage} alt="badge" />
        </div>
        <h2 className={styles.title}>{title}</h2>
      </div>
    </a>
  );
};

Widget.propTypes = {
  background: PropTypes.string,
  badgeImage: PropTypes.string,
  badgeColor: PropTypes.string,
  title: PropTypes.string,
  link: PropTypes.string,
};

Widget.defaultProps = {
  background: '',
  badgeImage: '',
  badgeColor: '',
  title: '',
  link: '',
};

export default Widget;
