import React from 'react';
import Grid from '@material-ui/core/Grid';

import styles from './Footer.module.scss';

const Footer = () => (
  <div className={styles.footer}>
    <Grid container spacing={3}>
      <Grid item xs={6} className={styles.wrapper}>
        <img src="images/icon-marketplace.svg" alt="marketplace" />
        <h2 className={styles.title}>
          List your tool on
          <br />
          GitHub Marketplace
        </h2>
      </Grid>
      <Grid item xs={3}>
        <section className={styles.section}>
          <h3 className={styles.subtitle}>Read the documentation</h3>
          <p className={styles.comment}>
            Learn how you can build tools to extend and improve developers'
            workflows.
          </p>
        </section>
      </Grid>
      <Grid item xs={3}>
        <section className={styles.section}>
          <h3 className={styles.subtitle}>Submit your tool for review</h3>
          <p className={styles.comment}>
            Share your app or GitHub Action with millions of developers.
          </p>
        </section>
      </Grid>
    </Grid>
  </div>
);

export default Footer;
