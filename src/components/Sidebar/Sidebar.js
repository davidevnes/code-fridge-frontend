import React from 'react';

import List from '../List';
import Dropdown from '../Dropdown';

import * as consts from './consts';
import styles from './Sidebar.module.scss';

const Sidebar = () => (
  <div className={styles.sidebar}>
    <List items={consts.TYPES} title="Types" />
    <List items={consts.CATEGORIES} title="Categories" />
    <Dropdown items={consts.FILTERS} title="Filters" />
    <List items={consts.VERIFICATION} title="Verifications" />
    <Dropdown items={consts.YOUR_ITEMS} title="Your Items" />
  </div>
);

export default Sidebar;
