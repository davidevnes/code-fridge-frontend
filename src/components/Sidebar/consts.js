export const TYPES = ['Apps', 'Actions'];
export const CATEGORIES = [
  'API management',
  'Chat',
  'Code quality',
  'Code review',
  'Continuous integration',
  'Dependency management',
  'Deployment',
  'IDEs',
  'Learning',
  'Localization',
  'Mobile',
  'Monitoring',
  'Project management',
  'Publishing',
  'Recently added',
  'Security',
  'Support',
  'Testing',
  'Utilities',
];
export const FILTERS = ['Free', 'Free Trials', 'GitHub Enterprise', 'Paid'];
export const VERIFICATION = ['Verified', 'Unverified'];
export const YOUR_ITEMS = ['Pending Orders'];
