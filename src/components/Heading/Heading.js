import React from 'react';

import Button from '../CustomButton';

import styles from './Heading.module.scss';

const Heading = () => (
  <div className={styles.heading}>
    <h1 className={styles.title}>Extend GitHub</h1>
    <h3 className={styles.description}>Find tools to improve your workflow</h3>
    <Button>Explore free apps</Button>
  </div>
);

export default Heading;
