export const ACTIONS = {
  image: '/images/action-icon-white.png',
  title: 'Introducing GitHub Actions',
  description: 'An entirely new way to automate your development workflow.',
  button: 'Explore Actions',
};

export const RECOMMENDS = {
  title: 'Recommended for you',
  widgets: [
    {
      image: '/images/widgets/1.png',
      color: '#242C33',
      background: '#fff',
      title: 'Codacy',
    },
    {
      image: '/images/widgets/1.png',
      color: '#242C33',
      background: '#fff',
      title: 'Codacy',
    },
    {
      image: '/images/widgets/1.png',
      color: '#242C33',
      background: '#fff',
      title: 'Codacy',
    },
    {
      image: '/images/widgets/1.png',
      color: '#242C33',
      background: '#fff',
      title: 'Codacy',
    },
  ],
};

export const CATEGORIES = [
  {
    title: 'Trending',
    tiles: [
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
        link: '#',
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
      },
    ],
  },
  {
    title: 'Recently added',
    description:
      'The latest tools that help you and your team build software better, together',
    link: '#',
    tiles: [
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
      },
      {
        image: '/images/widgets/1.png',
        color: '#242C33',
        title: 'Check Run Reporter',
        comment:
          'Your CI service tells you that tests failed; Check Run Reporter tells you *which* tests failed',
        verified: true,
      },
    ],
  },
];
