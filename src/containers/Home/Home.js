import React from 'react';
import get from 'lodash/get';
import Widget from 'components/Widget';
import Panel from 'components/Panel';
import AdWidget from 'components/AdWidget';
import TileItem from 'components/TileItem';

import { ACTIONS, RECOMMENDS, CATEGORIES } from './consts';

const Home = () => {
  return (
    <>
      <AdWidget
        imagePath={ACTIONS.image}
        title={ACTIONS.title}
        description={ACTIONS.description}
        buttonValue={ACTIONS.button}
      />
      <Panel title={RECOMMENDS.title}>
        {RECOMMENDS.widgets.map((widget) => (
          <Widget
            key={widget.title}
            badgeImage={widget.image}
            badgeColor={widget.color}
            title={widget.title}
          />
        ))}
      </Panel>
      {CATEGORIES.map((category) => (
        <Panel
          key={category.title}
          title={category.title}
          link={get(category, 'link')}
          description={get(category, 'description')}
        >
          {category.tiles.map((tile) => (
            <TileItem
              key={tile.title}
              badgeImage={tile.image}
              badgeColor={tile.color}
              title={tile.title}
              comment={tile.comment}
              verfied={get(tile, 'verified')}
              link={get(tile, 'link')}
            />
          ))}
        </Panel>
      ))}
    </>
  );
};

export default Home;
