import React from 'react';
import get from 'lodash/get';
import { v1 as uuid } from 'uuid';
import { useSelector, useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from 'components/CustomButton';
import TileItem from 'components/TileItem';
import { filterDataRequested } from 'redux/search/actions';

import styles from './Search.module.scss';

const Search = () => {
  const disptach = useDispatch();
  const { loading, data } = useSelector((state) => state.search);

  const addMoreHandler = () => {
    disptach(filterDataRequested(data.query, data.first, data.after));
  };

  return (
    <div className={styles.search}>
      <Grid container spacing={3}>
        {data.labels.map((label) => {
          return (
            <Grid key={uuid()} item xs={6}>
              <TileItem
                badgeImage={get(
                  label,
                  'node.discogs.images[0].url',
                  'images/widgets/temp.png'
                )}
                badgeColor="#fff"
                title={label.node.name}
                comment={get(label, 'node.discogs.contactInfo', '---')}
              />
            </Grid>
          );
        })}
      </Grid>
      {loading && <CircularProgress className={styles.loading} />}
      {data.query && !loading && (
        <Button
          variant={styles.button}
          onClick={addMoreHandler}
          disabled={data.after.endCursor === null}
        >
          Add more
        </Button>
      )}
    </div>
  );
};

export default Search;
