import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Switch, Route, useHistory } from 'react-router-dom';
import Input from 'components/CustomInput';
import { useForm } from 'react-hook-form';
import Home from 'containers/Home';
import Search from 'containers/Search';
import { filterDataRequested } from 'redux/search/actions';

const Content = () => {
  const limit = useSelector((state) => state.search.data.first);
  const dispatch = useDispatch();
  const history = useHistory();
  const { register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    dispatch(filterDataRequested(data.query, limit, {}));
    history.push('/search');
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input
          placeholder="Search for music labels"
          name="query"
          inputRef={register({ required: true })}
        />
      </form>
      <Switch>
        <Route path="/search">
          <Search />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </>
  );
};

export default Content;
