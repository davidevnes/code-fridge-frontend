import { createReducer } from '@reduxjs/toolkit';

import * as actionsTypes from './actionsTypes';

const initialState = {
  loading: false,
  account: null,
  token: null,
  email: '',
};

// create reducer comes with immer produce so we don't need to take care of immutational update
const authReducer = createReducer(initialState, {
  [actionsTypes.SIGNIN_REQUESTED]: (state) => {
    state.loading = true;
  },
  [actionsTypes.SIGNIN_SUCCEEDED]: (state) => {
    state.loading = false;
  },
  [actionsTypes.SIGNIN_FAILED]: (state) => {
    state.loading = false;
  },
});

export default authReducer;
