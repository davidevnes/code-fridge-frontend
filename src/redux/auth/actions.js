import { SIGNIN_REQUESTED, SIGNIN_SUCCEEDED, SIGNIN_FAILED } from './actionsTypes';

export const signInRequested = (payload) => {
  return {
    type: SIGNIN_REQUESTED,
    payload,
  };
};

export const signInSucceeded = (payload) => {
  return {
    type: SIGNIN_SUCCEEDED,
    payload,
  };
};

export const signInFailed = (payload) => {
  return {
    type: SIGNIN_FAILED,
    payload,
  };
};
