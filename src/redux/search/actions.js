import {
  FILTER_DATA_REQUESTED,
  FILTER_DATA_SUCCEEDED,
  FILTER_DATA_FAILED,
} from './actionsTypes';

export const filterDataRequested = (query, first, after) => {
  return {
    type: FILTER_DATA_REQUESTED,
    payload: {
      query,
      first,
      after,
    },
  };
};

export const filterDataSucceeded = ({ search }) => {
  return {
    type: FILTER_DATA_SUCCEEDED,
    payload: {
      labels: search.labels,
    },
  };
};

export const filterDataFailed = (error) => {
  return {
    type: FILTER_DATA_FAILED,
    payload: {
      error,
    },
  };
};
