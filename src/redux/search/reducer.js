import { createReducer } from '@reduxjs/toolkit';

import * as actionsTypes from './actionsTypes';

const initialState = {
  loading: false,
  data: {
    labels: [],
    query: '',
    first: 10,
    after: {},
  },
  error: null,
};

// create reducer comes with immer produce so we don't need to take care of immutational update
const searchReducer = createReducer(initialState, {
  [actionsTypes.FILTER_DATA_REQUESTED]: (state, action) => {
    if (action.payload.after.endCursor === undefined) {
      state.data.labels = [];
    }
    state.loading = true;
    state.data.query = action.payload.query;
    state.data.first = action.payload.first;
    state.data.after = action.payload.after;
  },
  [actionsTypes.FILTER_DATA_SUCCEEDED]: (state, action) => {
    state.loading = false;
    state.data.labels.push(...action.payload.labels.edges);
    state.data.after = action.payload.labels.pageInfo;
  },
  [actionsTypes.FILTER_DATA_FAILED]: (state, action) => {
    state.loading = false;
    state.error = action.payload.error;
  },
});

export default searchReducer;
