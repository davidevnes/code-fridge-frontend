import React from 'react';
import Grid from '@material-ui/core/Grid';
import Navbar from 'components/Navbar';
import Heading from 'components/Heading';
import Sidebar from 'components/Sidebar';
import Content from 'containers/Content';
import Footer from 'components/Footer';

import 'App.css';

function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="App-container">
        <Heading />
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <Sidebar />
          </Grid>
          <Grid item xs={9}>
            <Content />
          </Grid>
        </Grid>
        <Footer />
      </div>
    </div>
  );
}

export default App;
