import { put, takeLatest } from 'redux-saga/effects';
import { SIGNIN_REQUESTED } from 'redux/auth/actionsTypes';
import { signInFailed, signInSucceeded } from 'redux/auth/actions';

function* signIn() {
  try {
    const response = {};
    yield put(signInSucceeded(response));
  } catch (error) {
    yield put(signInFailed(error));
  }
}

function* authSaga() {
  yield takeLatest(SIGNIN_REQUESTED, signIn);
}

export default authSaga;
