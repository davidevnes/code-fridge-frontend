import { call, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash/get';
import { FILTER_DATA_REQUESTED } from 'redux/search/actionsTypes';
import { filterDataSucceeded, filterDataFailed } from 'redux/search/actions';
import { createQuery } from 'providers/apollo';

import { FILTER_MUSIC_LABELS } from './gql';

function* filterMusicLabels(action) {
  try {
    const { query, first, after } = action.payload;
    const endCursor = get(after, 'endCursor');
    const response = yield call(createQuery, FILTER_MUSIC_LABELS, {
      query,
      first,
      after: endCursor,
    });
    yield put(filterDataSucceeded(response.data));
  } catch (error) {
    yield put(filterDataFailed(error));
  }
}

function* searchSaga() {
  yield takeLatest(FILTER_DATA_REQUESTED, filterMusicLabels);
}

export default searchSaga;
