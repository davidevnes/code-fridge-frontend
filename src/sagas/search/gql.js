import gql from 'graphql-tag';

export const FILTER_MUSIC_LABELS = gql`
  query FilterMusicLabels($query: String!, $first: Int!, $after: String) {
    search {
      labels(query: $query, first: $first, after: $after) {
        ... on LabelConnection {
          pageInfo {
            endCursor
          }
          edges {
            cursor
            score
            node {
              mbid
              name
              type
              discogs {
                contactInfo
                images {
                  url
                }
              }
            }
          }
        }
      }
    }
  }
`;
