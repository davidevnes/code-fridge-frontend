import { all } from 'redux-saga/effects';

import authSaga from './auth';
import searchSaga from './search';

export default function* rootSaga() {
  yield all([authSaga(), searchSaga()]);
}
